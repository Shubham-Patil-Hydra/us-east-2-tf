terraform {
    backend "s3" {
        bucket = "terraform321"
        region = "us-east-2"
        key = "terraform.tfstate"
    }
}

provider "aws"{
    region = "us-east-2"
}
resource "aws_instance" "useasttfinstance" {
      ami = "ami-0747d5e46a4edf541"
    instance_type = var.instance_type
    key_name = var.key_pair
    tags = {
        name = "Shubham"
        env = "devops"
    }
    vpc_security_group_ids = ["sg-08ad2e43a6764bf58" , "sg-090c14ae715573e6f"]
}