variable "key_pair" {
    description = "my key pair"
    default = "US-East-02"
  
}
variable "instance_type" {
    description = "Instance Type:"
    default = "t2.micro"
  
}

variable "region" {
    description = "This is my home region"
    default = "us-east-2"
  
}